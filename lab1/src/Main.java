import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Sum");
        Scanner sc = new Scanner(System.in);
        System.out.printf("Input a = ");
        int a = sc.nextInt();
        System.out.printf("Input b = ");
        int b = sc.nextInt();
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("a + b = " + new Main().sum(a, b));
    }
    
    int sum(int a, int b) {
        System.out.println("Function \"sum\"");
        return a + b;
    }
}